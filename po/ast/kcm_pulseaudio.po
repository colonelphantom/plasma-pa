# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the plasma-pa package.
#
# Enol P. <enolp@softastur.org>, 2023.
msgid ""
msgstr ""
"Project-Id-Version: plasma-pa\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-05-07 02:16+0000\n"
"PO-Revision-Date: 2023-05-23 23:12+0200\n"
"Last-Translator: Enol P. <enolp@softastur.org>\n"
"Language-Team: \n"
"Language: ast\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 23.04.1\n"

#: context.cpp:596
#, kde-format
msgctxt "Name shown in debug pulseaudio tools"
msgid "Plasma PA"
msgstr "Plasma PA"

#: kcm/ui/CardListItem.qml:52 kcm/ui/DeviceListItem.qml:134
#, kde-format
msgctxt "@label"
msgid "Profile:"
msgstr "Perfil:"

#: kcm/ui/DeviceListItem.qml:47
#, kde-format
msgctxt "label of device items"
msgid "%1 (%2)"
msgstr "%1 (%2)"

#: kcm/ui/DeviceListItem.qml:88
#, kde-format
msgid "Port:"
msgstr "Puertu:"

#: kcm/ui/DeviceListItem.qml:111 qml/listitemmenu.cpp:387
#, kde-format
msgctxt "Port is unavailable"
msgid "%1 (unavailable)"
msgstr "%1 (nun ta disponible)"

#: kcm/ui/DeviceListItem.qml:113 qml/listitemmenu.cpp:389
#, kde-format
msgctxt "Port is unplugged"
msgid "%1 (unplugged)"
msgstr ""

#: kcm/ui/DeviceListItem.qml:194
#, kde-format
msgctxt "Placeholder is channel name"
msgid "%1:"
msgstr "%1:"

#: kcm/ui/DeviceListItem.qml:227
#, kde-format
msgctxt "Perform an audio test of the device"
msgid "Test"
msgstr "Probar"

#: kcm/ui/DeviceListItem.qml:236
#, kde-format
msgctxt "Audio balance (e.g. control left/right volume individually"
msgid "Balance"
msgstr "Balance"

#: kcm/ui/main.qml:82
#, kde-format
msgid "Show Inactive Devices"
msgstr ""

#: kcm/ui/main.qml:89
#, kde-format
msgid "Configure Volume Controls…"
msgstr "Configurar los controles del volume…"

#: kcm/ui/main.qml:96
#, kde-format
msgid "Configure…"
msgstr "Configurar…"

#: kcm/ui/main.qml:99
#, kde-format
msgid "Requires %1 PulseAudio module"
msgstr ""

#: kcm/ui/main.qml:102
#, kde-format
msgid ""
"Add virtual output device for simultaneous output on all local sound cards"
msgstr ""

#: kcm/ui/main.qml:108
#, kde-format
msgid ""
"Automatically switch all running streams when a new output becomes available"
msgstr ""

#: kcm/ui/main.qml:145
#, kde-format
msgid "Playback Devices"
msgstr "Preseos de reproducción"

#: kcm/ui/main.qml:169
#, kde-format
msgid "Recording Devices"
msgstr "Preseos de grabación"

#: kcm/ui/main.qml:193
#, kde-format
msgid "Inactive Cards"
msgstr ""

#: kcm/ui/main.qml:227
#, kde-format
msgid "Playback Streams"
msgstr ""

#: kcm/ui/main.qml:276
#, kde-format
msgid "Recording Streams"
msgstr ""

#: kcm/ui/main.qml:310
#, kde-format
msgctxt ""
"%1 is an error string produced by an external component, and probably "
"untranslated"
msgid ""
"Error trying to play a test sound. \n"
"The system said: \"%1\""
msgstr ""

#: kcm/ui/main.qml:406
#, kde-format
msgid "Front Left"
msgstr ""

#: kcm/ui/main.qml:407
#, kde-format
msgid "Front Center"
msgstr ""

#: kcm/ui/main.qml:408
#, kde-format
msgid "Front Right"
msgstr ""

#: kcm/ui/main.qml:409
#, kde-format
msgid "Side Left"
msgstr ""

#: kcm/ui/main.qml:410
#, kde-format
msgid "Side Right"
msgstr ""

#: kcm/ui/main.qml:411
#, kde-format
msgid "Rear Left"
msgstr ""

#: kcm/ui/main.qml:412
#, kde-format
msgid "Subwoofer"
msgstr ""

#: kcm/ui/main.qml:413
#, kde-format
msgid "Rear Right"
msgstr ""

#: kcm/ui/main.qml:414
#, kde-format
msgid "Mono"
msgstr ""

#: kcm/ui/main.qml:468
#, kde-format
msgid "Click on any speaker to test sound"
msgstr "Calca en cualesquier altavoz pa probar el soníu"

#: kcm/ui/MuteButton.qml:22
#, kde-format
msgctxt "Unmute audio stream"
msgid "Unmute %1"
msgstr ""

#: kcm/ui/MuteButton.qml:22
#, kde-format
msgctxt "Mute audio stream"
msgid "Mute %1"
msgstr ""

#: kcm/ui/StreamListItem.qml:57
#, kde-format
msgid "Notification Sounds"
msgstr ""

#: kcm/ui/StreamListItem.qml:63
#, kde-format
msgctxt "label of stream items"
msgid "%1: %2"
msgstr "%1: %2"

#: kcm/ui/VolumeControlsConfig.qml:15
#, kde-format
msgid "Volume Controls"
msgstr ""

#: kcm/ui/VolumeControlsConfig.qml:31
#, kde-format
msgid "Raise maximum volume"
msgstr ""

#: kcm/ui/VolumeControlsConfig.qml:39
#, kde-format
msgid "Volume change step:"
msgstr ""

#: kcm/ui/VolumeControlsConfig.qml:63
#, kde-format
msgid "Play audio feedback for changes to:"
msgstr ""

#: kcm/ui/VolumeControlsConfig.qml:64 kcm/ui/VolumeControlsConfig.qml:73
#, kde-format
msgid "Audio volume"
msgstr "Volume del audiu"

#: kcm/ui/VolumeControlsConfig.qml:72
#, kde-format
msgid "Show visual feedback for changes to:"
msgstr ""

#: kcm/ui/VolumeControlsConfig.qml:80
#, kde-format
msgid "Microphone sensitivity"
msgstr "Sensibilidá del micrófonu"

#: kcm/ui/VolumeControlsConfig.qml:87
#, kde-format
msgid "Mute state"
msgstr ""

#: kcm/ui/VolumeControlsConfig.qml:94
#, kde-format
msgid "Default output device"
msgstr ""

#: kcm/ui/VolumeSlider.qml:51
#, kde-format
msgctxt "volume percentage"
msgid "%1%"
msgstr "%1%"

#: kcm/ui/VolumeSlider.qml:71
#, kde-format
msgctxt "only used for sizing, should be widest possible string"
msgid "100%"
msgstr "100%"

#: qml/listitemmenu.cpp:342
#, kde-format
msgid "Play all audio via this device"
msgstr ""

#: qml/listitemmenu.cpp:347
#, kde-format
msgid "Record all audio via this device"
msgstr ""

#: qml/listitemmenu.cpp:375
#, kde-format
msgctxt ""
"Heading for a list of ports of a device (for example built-in laptop "
"speakers or a plug for headphones)"
msgid "Ports"
msgstr ""

#: qml/listitemmenu.cpp:445
#, kde-format
msgctxt ""
"Heading for a list of device profiles (5.1 surround sound, stereo, speakers "
"only, ...)"
msgid "Profiles"
msgstr ""

#: qml/listitemmenu.cpp:479
#, kde-format
msgctxt ""
"Heading for a list of possible output devices (speakers, headphones, ...) to "
"choose"
msgid "Play audio using"
msgstr ""

#: qml/listitemmenu.cpp:481
#, kde-format
msgctxt ""
"Heading for a list of possible input devices (built-in microphone, "
"headset, ...) to choose"
msgid "Record audio using"
msgstr ""

#: qml/microphoneindicator.cpp:102
#, kde-format
msgid "Mute"
msgstr ""

#: qml/microphoneindicator.cpp:132 qml/microphoneindicator.cpp:134
#, kde-format
msgid "Microphone"
msgstr "Micrófonu"

#: qml/microphoneindicator.cpp:134
#, kde-format
msgid "Microphone Muted"
msgstr ""

#: qml/microphoneindicator.cpp:284
#, kde-format
msgctxt "list separator"
msgid ", "
msgstr ""

#: qml/microphoneindicator.cpp:284
#, kde-format
msgctxt "List of apps is using mic"
msgid "%1 are using the microphone"
msgstr ""

#: qml/microphoneindicator.cpp:310
#, kde-format
msgctxt "App %1 is using mic with name %2"
msgid "%1 is using the microphone (%2)"
msgstr ""

#: qml/microphoneindicator.cpp:317
#, kde-format
msgctxt "App is using mic"
msgid "%1 is using the microphone"
msgstr ""
