# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the plasma-pa package.
#
# Kishore G <kishore96@gmail.com>, 2021, 2022.
msgid ""
msgstr ""
"Project-Id-Version: plasma-pa\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-05-07 02:16+0000\n"
"PO-Revision-Date: 2022-11-03 21:31+0530\n"
"Last-Translator: Kishore G <kishore96@gmail.com>\n"
"Language-Team: Tamil <kde-i18n-doc@kde.org>\n"
"Language: ta\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 22.08.2\n"

#: context.cpp:596
#, kde-format
msgctxt "Name shown in debug pulseaudio tools"
msgid "Plasma PA"
msgstr "பிளாஸ்மா PA"

#: kcm/ui/CardListItem.qml:52 kcm/ui/DeviceListItem.qml:134
#, kde-format
msgctxt "@label"
msgid "Profile:"
msgstr "பயன்முறை:"

#: kcm/ui/DeviceListItem.qml:47
#, kde-format
msgctxt "label of device items"
msgid "%1 (%2)"
msgstr "%1 (%2)"

#: kcm/ui/DeviceListItem.qml:88
#, kde-format
msgid "Port:"
msgstr "புறை:"

#: kcm/ui/DeviceListItem.qml:111 qml/listitemmenu.cpp:387
#, kde-format
msgctxt "Port is unavailable"
msgid "%1 (unavailable)"
msgstr "%1 (கிட்டவில்லை)"

#: kcm/ui/DeviceListItem.qml:113 qml/listitemmenu.cpp:389
#, kde-format
msgctxt "Port is unplugged"
msgid "%1 (unplugged)"
msgstr "%1 (செருகப்படவில்லை)"

#: kcm/ui/DeviceListItem.qml:194
#, kde-format
msgctxt "Placeholder is channel name"
msgid "%1:"
msgstr "%1:"

#: kcm/ui/DeviceListItem.qml:227
#, kde-format
msgctxt "Perform an audio test of the device"
msgid "Test"
msgstr "சோதி"

#: kcm/ui/DeviceListItem.qml:236
#, kde-format
msgctxt "Audio balance (e.g. control left/right volume individually"
msgid "Balance"
msgstr "சமப்படுத்து"

#: kcm/ui/main.qml:82
#, kde-format
msgid "Show Inactive Devices"
msgstr "செயல்படாத சாதனங்களை காட்டு"

#: kcm/ui/main.qml:89
#, kde-format
msgid "Configure Volume Controls…"
msgstr "ஒலியளவு கட்டுப்படுத்திகளை அமை..."

#: kcm/ui/main.qml:96
#, kde-format
msgid "Configure…"
msgstr "அமை..."

#: kcm/ui/main.qml:99
#, kde-format
msgid "Requires %1 PulseAudio module"
msgstr "%1 PulseAudio கூறு இதற்கு தேவை"

#: kcm/ui/main.qml:102
#, kde-format
msgid ""
"Add virtual output device for simultaneous output on all local sound cards"
msgstr ""
"அனைத்து ஒலியட்டைகளிலும் ஒரே நேரத்தில் ஒலியை வெளியிடும் மெய்நிகர் வெளியீட்டு சாதனத்தை சேர்"

#: kcm/ui/main.qml:108
#, kde-format
msgid ""
"Automatically switch all running streams when a new output becomes available"
msgstr "புதிய வெளியீட்டு சாதனம் கிடைக்கும்போது அனைத்து செயலிகளின் ஒலியை அதற்கே மாற்று"

#: kcm/ui/main.qml:145
#, kde-format
msgid "Playback Devices"
msgstr "ஒலியியக்கும் சாதனங்கள்"

#: kcm/ui/main.qml:169
#, kde-format
msgid "Recording Devices"
msgstr "ஒலிப்பதிவு சாதனங்கள்"

#: kcm/ui/main.qml:193
#, kde-format
msgid "Inactive Cards"
msgstr "செயலிலில்லாத அட்டைகள்"

#: kcm/ui/main.qml:227
#, kde-format
msgid "Playback Streams"
msgstr "ஒலியியக்கும் செயலிகள்"

#: kcm/ui/main.qml:276
#, kde-format
msgid "Recording Streams"
msgstr "ஒலிப்பதிவு செய்யும் செயலிகள்"

#: kcm/ui/main.qml:310
#, kde-format
msgctxt ""
"%1 is an error string produced by an external component, and probably "
"untranslated"
msgid ""
"Error trying to play a test sound. \n"
"The system said: \"%1\""
msgstr ""
"சோதனைக்கான ஒலியை இசைப்பதில் சிக்கல். \n"
"கணினி கூறியது: \"%1\""

#: kcm/ui/main.qml:406
#, kde-format
msgid "Front Left"
msgstr "முன்புறம் இடது"

#: kcm/ui/main.qml:407
#, kde-format
msgid "Front Center"
msgstr "முன்புறம் நடு"

#: kcm/ui/main.qml:408
#, kde-format
msgid "Front Right"
msgstr "முன்புறம் வலது"

#: kcm/ui/main.qml:409
#, kde-format
msgid "Side Left"
msgstr "இடது ஓரம்"

#: kcm/ui/main.qml:410
#, kde-format
msgid "Side Right"
msgstr "வலது ஓரம்"

#: kcm/ui/main.qml:411
#, kde-format
msgid "Rear Left"
msgstr "பின்புறம் இடது"

#: kcm/ui/main.qml:412
#, kde-format
msgid "Subwoofer"
msgstr ""

#: kcm/ui/main.qml:413
#, kde-format
msgid "Rear Right"
msgstr "பின்புறம் வலது"

#: kcm/ui/main.qml:414
#, kde-format
msgid "Mono"
msgstr ""

#: kcm/ui/main.qml:468
#, kde-format
msgid "Click on any speaker to test sound"
msgstr "ஒலியை சோதிக்க ஏதாவதொரு ஒலிபெருக்கியில் கிளிக் செய்யுங்கள்"

#: kcm/ui/MuteButton.qml:22
#, kde-format
msgctxt "Unmute audio stream"
msgid "Unmute %1"
msgstr "%1-இன் ஒலியை அனுமதி"

#: kcm/ui/MuteButton.qml:22
#, kde-format
msgctxt "Mute audio stream"
msgid "Mute %1"
msgstr "%1-இன் ஒலியை அடக்கு"

#: kcm/ui/StreamListItem.qml:57
#, kde-format
msgid "Notification Sounds"
msgstr "அறிவிப்பு ஒலிகள்"

#: kcm/ui/StreamListItem.qml:63
#, kde-format
msgctxt "label of stream items"
msgid "%1: %2"
msgstr "%1: %2"

#: kcm/ui/VolumeControlsConfig.qml:15
#, kde-format
msgid "Volume Controls"
msgstr "ஒலியளவு கட்டுப்படுத்திகள்"

#: kcm/ui/VolumeControlsConfig.qml:31
#, kde-format
msgid "Raise maximum volume"
msgstr "அதிகபட்ச ஒலியை உயர்த்து"

#: kcm/ui/VolumeControlsConfig.qml:39
#, kde-format
msgid "Volume change step:"
msgstr "ஒலியளவை மாற்றுவதற்கான படி:"

#: kcm/ui/VolumeControlsConfig.qml:63
#, kde-format
msgid "Play audio feedback for changes to:"
msgstr "இம்மாற்றங்களுக்கு ஒலியாக பின்னூட்டம் தா:"

#: kcm/ui/VolumeControlsConfig.qml:64 kcm/ui/VolumeControlsConfig.qml:73
#, kde-format
msgid "Audio volume"
msgstr "ஒலியின் அளவு"

#: kcm/ui/VolumeControlsConfig.qml:72
#, kde-format
msgid "Show visual feedback for changes to:"
msgstr "இம்மாற்றங்களுக்கு காட்சிமுறையாக பின்னூட்டம் தா:"

#: kcm/ui/VolumeControlsConfig.qml:80
#, kde-format
msgid "Microphone sensitivity"
msgstr "ஒலிவாங்கியின் உணர்வுத்திறன்"

#: kcm/ui/VolumeControlsConfig.qml:87
#, kde-format
msgid "Mute state"
msgstr "ஒலியடக்க நிலை"

#: kcm/ui/VolumeControlsConfig.qml:94
#, kde-format
msgid "Default output device"
msgstr "இயல்பிருப்பு வெளியீடு சாதனம்"

#: kcm/ui/VolumeSlider.qml:51
#, kde-format
msgctxt "volume percentage"
msgid "%1%"
msgstr "%1%"

#: kcm/ui/VolumeSlider.qml:71
#, kde-format
msgctxt "only used for sizing, should be widest possible string"
msgid "100%"
msgstr "100%"

#: qml/listitemmenu.cpp:342
#, kde-format
msgid "Play all audio via this device"
msgstr "எல்லா ஒலிகளையும் இதன் வழியாக இயக்கு"

#: qml/listitemmenu.cpp:347
#, kde-format
msgid "Record all audio via this device"
msgstr "எல்லா ஒலிகளையும் இதன் வழியாக பதிவு செய்"

#: qml/listitemmenu.cpp:375
#, kde-format
msgctxt ""
"Heading for a list of ports of a device (for example built-in laptop "
"speakers or a plug for headphones)"
msgid "Ports"
msgstr "புறைகள்"

#: qml/listitemmenu.cpp:445
#, kde-format
msgctxt ""
"Heading for a list of device profiles (5.1 surround sound, stereo, speakers "
"only, ...)"
msgid "Profiles"
msgstr "பயன்முறைகள்"

#: qml/listitemmenu.cpp:479
#, kde-format
msgctxt ""
"Heading for a list of possible output devices (speakers, headphones, ...) to "
"choose"
msgid "Play audio using"
msgstr "இதனை கொண்டு ஒலியை இயக்கு"

#: qml/listitemmenu.cpp:481
#, kde-format
msgctxt ""
"Heading for a list of possible input devices (built-in microphone, "
"headset, ...) to choose"
msgid "Record audio using"
msgstr "இதனை கொண்டு ஒலிப்பதிவு செய்"

#: qml/microphoneindicator.cpp:102
#, kde-format
msgid "Mute"
msgstr "ஒலியை அடக்கு"

#: qml/microphoneindicator.cpp:132 qml/microphoneindicator.cpp:134
#, kde-format
msgid "Microphone"
msgstr "ஒலிவாங்கி"

#: qml/microphoneindicator.cpp:134
#, kde-format
msgid "Microphone Muted"
msgstr "ஒலிவாங்கி அடக்கப்பட்டது"

#: qml/microphoneindicator.cpp:284
#, kde-format
msgctxt "list separator"
msgid ", "
msgstr ", "

#: qml/microphoneindicator.cpp:284
#, kde-format
msgctxt "List of apps is using mic"
msgid "%1 are using the microphone"
msgstr "%1 ஒலிவாங்கியை பயன்படுத்துகின்றன"

#: qml/microphoneindicator.cpp:310
#, kde-format
msgctxt "App %1 is using mic with name %2"
msgid "%1 is using the microphone (%2)"
msgstr "%1 ஒலிவாங்கியை பயன்படுத்துகிறது (%2)"

#: qml/microphoneindicator.cpp:317
#, kde-format
msgctxt "App is using mic"
msgid "%1 is using the microphone"
msgstr "%1 ஒலிவாங்கியை பயன்படுத்துகிறது"

#~ msgid "This module allows configuring the Pulseaudio sound subsystem."
#~ msgstr "Pulseaudio ஒலி அமைப்பை அமைக்க இந்தக் கூறு உதவும்."

#~ msgid "100%"
#~ msgstr "100%"

#~ msgctxt "@title"
#~ msgid "Audio"
#~ msgstr "ஒலி"

#~ msgctxt "@info:credit"
#~ msgid "Author"
#~ msgstr "இயற்றியவர்"

#~ msgid "Configure"
#~ msgstr "அமை"

#~ msgid "Device Profiles"
#~ msgstr "சாதனப் பயன்முறைகள்"

#~ msgid "Advanced Output Configuration"
#~ msgstr "மேம்பட்ட வெளியீட்டு அமைப்பு"

#~ msgid "Speaker Placement and Testing"
#~ msgstr "ஒலிபெருக்கியின் இருப்பிடம் மற்றும் சோதனை"

#~ msgctxt "@label"
#~ msgid "Output:"
#~ msgstr "வெளியீடு:"

#~ msgctxt "Port is unavailable"
#~ msgid " (unavailable)"
#~ msgstr "(கிட்டவில்லை)"

#~ msgctxt "Port is unplugged"
#~ msgid " (unplugged)"
#~ msgstr "(செருகப்படவில்லை)"
